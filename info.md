# IMPACT Initiatives

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/impact-initiatives/impact-initiatives.git
git branch -M main
git push -uf origin main
```

---

## Improving respondent selection for humanitarian research in difficult contexts

## Project Description

[Link](https://docs.google.com/document/d/1MpWWS1hrH38uwrqhQwdh2-WZOyqWMr25)

## Important links

[Google Drive](https://drive.google.com/drive/u/3/folders/16GdLCqvOD_L9nrP9ZNr1par7aUTB681K)  
[Polybox](https://polybox.ethz.ch/index.php/f/2556780901)  
[Gitlab (you're already there ;) )](https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/impact-initiatives)  
[Welcome booklet](https://drive.google.com/file/d/1YUVBDwezo1w1JPj26yAjec1hNtIaIxsX/view?usp=sharing)  
[LaTeX Document](https://www.overleaf.com/7772413865ffshyrtvsqcs)

## Team

**H4G project responsibles:** Pablo pablo.lahmann@analytics-club.org and Xiang xiang.li@analytics-club.org  
**NGO representative:** Olivier Cecchi  
**Mentor:** Nima Riahi nima.riahi@sbb.ch

**Team members:**

- Samyak Shah
- Ayoung Song
- Claus Wirnsperger
- Feichi Lu
