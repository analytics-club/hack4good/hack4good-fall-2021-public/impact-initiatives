"""main data engineering interface to prepare the design matrix for modeling"""
# pylint: disable=invalid-name
import os
import pickle
import numpy as np
import pandas as pd

from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder

from impact.pipelines import (
    KIMetaDataPipeline,
    KIScoringDataPipeline,
    QuestionsDataPipeline,
)


def load_master(path="/data/merged/master.pickle"):
    """load the master table"""

    with open(path, "rb") as fp:
        master = pickle.load(fp)
    return master


def save_dataframe(df, file_name, main_path):
    """save dataframe as pickle file"""
    with open(os.path.join(main_path, file_name), "wb") as fp:
        pickle.dump(df, fp)


def unify_questions(merged_edges, pilot="ner"):
    """
    Unifies all of the questions asked to the KIs, makes sure that all
    KIs have the same questions asked to them!

    Parameters:
    -----------
    merged_edges: pd.DataFrame
        the joined dataframe containing the edges and scoring/KI-meta/Question-meta data

    Returns:
    --------
    final_df: pd.DataFrame
        the final matrix where each KI has the same questions asked to all KIs
    """
    # choose the pilot KIs
    pilot = merged_edges[merged_edges.pilot == pilot]

    # intersect all of the questions
    shared_questions = None
    for _, group in pilot.groupby("code"):
        if shared_questions is None:
            shared_questions = set(group.question.unique())

        intersected_questions = set(group.question.unique()).intersection(
            shared_questions
        )

        shared_questions = intersected_questions

    # assert the selection
    for _, group in pilot.groupby("code"):
        assert len(group.question.unique()) == len(
            group
        ), "duplicate questions are present"
        for question in shared_questions:
            if question not in group.question.unique():
                raise ValueError("a question in shared_questions is not shared")

    shared_question_indices = [
        True if question in shared_questions else False for question in pilot.question
    ]
    final_df = pilot[shared_question_indices]
    return final_df


def engineer_features(df):
    """
    Custom feature engineering based on question and KI specific features & edges featuress
    """
    # question about displacement overlapping with KI displacement status?
    df["displacement_overlap"] = (df.question_population == df.statut).astype(int)

    # question about a sector overlapping with the KI connections (in & out)
    df["question_edges_overlap"] = [
        df[f"in_{sector}"].iloc[row_idx] + df[f"out_{sector}"].iloc[row_idx]
        for row_idx, sector in enumerate(df.question_sector)
    ]

    # get the absolute value of the scores column
    df["abs_value"] = np.abs(df["value"].astype("float"))

    return df


def collapse_on_kis(df):
    """
    Collapse the given dataframe ontop of its KIs and have one row per KI
    """
    sum_scores = []
    edges_overlap = []
    displacement_overlap = []

    new_df = pd.DataFrame()
    for _, group in df.groupby("code"):
        group = group.copy()

        # --- collect all of the timedependent features as a sum or avg for each KI

        # take the sum of abs-scores
        sum_scores.append(group.abs_value.sum())

        # take the sum for the edges overlap
        edges_overlap.append(group.question_edges_overlap.sum())

        # take the sum for the displacement overlap
        displacement_overlap.append(group.displacement_overlap.sum())

        # remove all columns that are non-unique per KI
        for column in group.columns:
            if len(group[column].unique()) != 1:
                group.drop(columns=column, inplace=True)

        new = group.iloc[:1]
        new_df = pd.concat((new_df, new), axis=0)

    new_df["question_edges_overlap"] = edges_overlap
    new_df["displacement_overlap"] = displacement_overlap
    new_df["score_sum"] = sum_scores

    return new_df


def main_pipeline(output_path="data/merged"):
    """
    main pipeline to load the files and merge the dataframes

    save the intermediate stages and the final "master" fully merged
    dataframe as pickle file
    """

    print("=== DATA ENGINEERING PIPELINE ===")
    print("Commencing Part 1 --- Data Loading ...")

    # --- KI meta data loading pipeline
    ki_meta = KIMetaDataPipeline().fit_transform("data/raw/KI_meta.xlsx")
    # drop otw. duplicate columns
    ki_meta = ki_meta.drop(columns=["pilot"])

    save_dataframe(ki_meta, "KI_meta.pickle", output_path)
    print("Done 1/5")

    # --- KI scoring data loading pipeline
    ki_scoring = KIScoringDataPipeline().fit_transform("data/raw/KI_scoring.xlsx")
    save_dataframe(ki_scoring, "KI_scoring.pickle", output_path)
    print("Done 2/5")

    # --- merge the KI meta data and the KI scoring data
    merged_on_ki = pd.merge(ki_scoring, ki_meta, on="code", how="inner")
    save_dataframe(merged_on_ki, "KI_scoring_meta.pickle", output_path)
    print("Done 3/5")

    # --- questions meta data loading pipeline
    questions_meta = QuestionsDataPipeline().fit_transform(
        "data/raw/Questions_meta_translated.xlsx"
    )
    save_dataframe(questions_meta, "Questions_meta.pickle", output_path)
    print("Done 4/5")

    # --- merge the KI info & scoring table with the questions meta data table
    merged_total = pd.merge(merged_on_ki, questions_meta, on="question", how="left")
    save_dataframe(merged_total, "master.pickle", output_path)
    print("Done 5/5")

    print(50 * "-")
    print("Merged master-table has shape: ", merged_total.shape)
    print("Commencing Part 2 --- Feature Engineering and Design Matrix Creation...")

    # add edges features for each individual KI
    edges_features = pd.read_excel("data/raw/KI_edges_feature.xlsx").drop(
        columns="Unnamed: 0"
    )

    # left join (keeping the keys on the left matrix) with merged_total and edges_features
    merged_with_edges = pd.merge(merged_total, edges_features, on="code", how="left")
    save_dataframe(merged_with_edges, "master_with_edges.pickle", output_path)

    # choose niger as the design matrix study
    unified = unify_questions(merged_with_edges, pilot="ner")
    save_dataframe(unified, "questions_unified.pickle", output_path)
    print("Done 1/4")

    # drop unnecessary columns from the question-filtered niger data
    unified.drop(
        columns=[
            "variable",
            "check",
            "QType",
            "age",
            "ki_specialization",
            "label",
            "label_translated",
            "inwomen",
            "comment",
            "Key",
            "Key_bck",
            "ki_data",
            "indicator",
            "polarise",
            "Unnamed: 20",
            "type",
            "rank",
            "ordinal_perc",
            "level",
            "q_choices",
            "pilot",
        ],
        inplace=True,
    )
    print("Done 2/4")

    # feature engineering of question & KI related features
    with_bonus_features = engineer_features(unified)

    # impute the numerical features that have NaN values
    has_nans = with_bonus_features[
        with_bonus_features.columns[with_bonus_features.isnull().sum() != 0]
    ]
    mean_imputer = SimpleImputer(strategy="mean")
    imputed = mean_imputer.fit_transform(has_nans.values)
    with_bonus_features[has_nans.columns] = imputed

    # collapse w.r.t. the KIs to take the sum of the scores across KIs
    niger_design = collapse_on_kis(with_bonus_features)

    # negligible profiles are renamed to other
    niger_design.profile_recoded = [
        "other"
        if profile in ("student", "cbo / ngo / local gov", "wash related")
        else profile
        for profile in niger_design.profile_recoded
    ]
    save_dataframe(niger_design, "niger_design.pickle", output_path)
    print("Done 3/4")

    # one hot encoding of categorical features
    enc = OneHotEncoder(drop="if_binary")
    cat_cols = niger_design.drop(columns="code").select_dtypes("object")
    encoded = enc.fit_transform(cat_cols).toarray()
    out_categories = (
        niger_design.quartier.unique().tolist()
        + ["nationalite", "sexe", "statut"]
        + niger_design.profile_recoded.unique().tolist()
    )
    categoricals = pd.DataFrame(encoded, columns=out_categories)
    non_categoricals = niger_design.select_dtypes(exclude="object").reset_index(
        drop=True
    )
    final = pd.concat((non_categoricals, categoricals), axis=1)

    # save the final feature matrix
    save_dataframe(final, "final.pickle", output_path)
    print("Done 4/4")

    print("Final design matrix for Niger study has shape", final.shape)


if __name__ == "__main__":
    main_pipeline()
