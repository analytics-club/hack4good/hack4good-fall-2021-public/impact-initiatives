"""data pipeline module"""
# pylint: disable=unused-argument, invalid-name
import pandas as pd
import numpy as np

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.impute import SimpleImputer


class ReplaceValues(BaseEstimator, TransformerMixin):
    """value replacement pipeline element"""

    def __init__(self, replaced, replacement):
        self._replaced = replaced
        self._replacement = replacement

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """replace the given values in the entire dataframe"""
        assert isinstance(
            X, pd.DataFrame
        ), "No pd.DataFrame given to ReplaceValues sklearn.estimator"
        return X.replace(self._replaced, self._replacement)


class LowerCase(BaseEstimator, TransformerMixin):
    """lower case transformation of string values in dataframes"""

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """lower case transformation on X"""
        assert isinstance(
            X, pd.DataFrame
        ), "No pd.DataFrame given to LowerCase sklearn.estimator"

        X = X.astype(str).apply(lambda p: p.str.lower())
        return X


class LoadExcel(BaseEstimator, TransformerMixin):
    """load the excel file from a given path"""

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """load the excel file"""
        return pd.read_excel(X)


class SelectColumns(BaseEstimator, TransformerMixin):
    """select a subset of columns in the dataframe"""

    def __init__(self, selected_columns):
        self._selected_columns = selected_columns

    def fit(self, X, y=None):
        """no fitting in this module"""
        return self

    def transform(self, X):
        """only select a subset of columns"""
        return X[self._selected_columns]


class RemoveColumns(BaseEstimator, TransformerMixin):
    """remove selected columns from the dataframe"""

    def __init__(self, removed_columns):
        self._removed_columns = removed_columns

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """remove the given columns from the dataframe"""
        return X.drop(columns=self._removed_columns)


class DropDuplicateRows(BaseEstimator, TransformerMixin):
    """remove duplicate rows in a dataframe"""

    def __init__(self, subset):
        self.subset = subset

    def fit(self, X, y=None):
        """no fitting in this module"""
        return self

    def transform(self, X):
        """remove the duplicate columns"""
        return X.drop_duplicates(subset=self.subset)


class ImputeDataFrame(BaseEstimator, TransformerMixin):
    """
    Imputation sklearn.estimator

    Works with pd.DataFrames
    """

    def __init__(self, strategy, data_type=None, columns=None):
        self._columns = columns
        self._imputer = SimpleImputer(missing_values=np.nan, strategy=strategy)
        self._data_type = data_type
        assert data_type in (
            "categorical",
            "numerical",
        ), "Data type neither categorical nor numerical"
        assert columns is not None or data_type is not None

    def fit(self, X, y=None):
        """fit the imputation method"""
        if self._data_type == "categorical":
            X = X.select_dtypes("object").copy()
            self._imputer.fit(X.values.reshape(-1, len(X.columns)))
        elif self._data_type == "numerical":
            X = X.select_dtypes(exclude="object").copy()
            self._imputer.fit(X.values.reshape(-1, len(X.columns)))
        else:
            self._imputer.fit(X[self._columns].values)

        return self

    def transform(self, X):
        """impute the dataframe"""
        if self._data_type == "categorical":
            columns = X.columns
            X_original = X.values
            cat = X.select_dtypes("object")
            indices = [idx for idx, col in enumerate(columns) if col in cat.columns]
            X_original[:, indices] = self._imputer.transform(cat.values)

        elif self._data_type == "numerical":
            columns = X.columns
            X_original = X.values
            cat = X.select_dtypes(exclude="object")
            indices = [idx for idx, col in enumerate(columns) if col in cat.columns]
            X_original[:, indices] = self._imputer.transform(cat.values)

        else:
            columns = X.columns
            X_original = X.values
            chosen_indices = [
                idx for idx, col in enumerate(columns) if col in self._columns
            ]
            X_original[:, chosen_indices] = self._imputer.transform(X.values)

        return pd.DataFrame(X_original, index=X.index, columns=X.columns)


class RenameColumn(BaseEstimator, TransformerMixin):
    """rename columns in a dataframe"""

    def __init__(self, old_columns, new_columns):
        self.old_columns = old_columns
        self.new_columns = new_columns
        assert len(self.old_columns) == len(self.new_columns)

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """rename columns"""
        for new_name, old_name in zip(self.new_columns, self.old_columns):
            X[new_name] = X[old_name].values
            X.drop(columns=old_name, inplace=True)
        return X


class RemoveDuplicateQuestions(BaseEstimator, TransformerMixin):
    """remove duplicate entries into the table for the same KI"""

    def fit(self, X, y=None):
        """nothing to fit"""
        return self

    def transform(self, X):
        """remove the duplicates"""

        for _, ki_group in X.groupby("code"):
            if len(ki_group.question.unique()) != len(ki_group):
                for _, question_group in ki_group.groupby("question"):
                    if len(question_group) != 1:
                        X.drop(index=question_group.index, inplace=True)

        return X


class UpdateValue(BaseEstimator, TransformerMixin):
    """Change some values of a column related to another column"""

    def __init__(self, refer_column, change_column, refer_value, change_value):
        self.refer_column = refer_column
        self.change_column = change_column
        self.refer_value = refer_value
        self.change_value = change_value
        assert isinstance(refer_column, str)
        assert isinstance(change_column, str)

    def fit(self, X, y=None):
        """no fitting in this estimator"""
        return self

    def transform(self, X):
        """update value"""
        assert isinstance(
            X, pd.DataFrame
        ), "No pd.DataFrame given to LowerCase sklearn.estimator"

        X.loc[
            X[self.refer_column] == self.refer_value, self.change_column
        ] = self.change_value
        return X
