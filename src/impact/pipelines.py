"""base data engineering pipelines for the xlsx files to prepare them for subsequent preprocessing"""
import numpy as np

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline

from impact.utils import (
    DropDuplicateRows,
    ReplaceValues,
    LoadExcel,
    RemoveColumns,
    RenameColumn,
    LowerCase,
    UpdateValue,
    RemoveDuplicateQuestions,
)


class KIMetaDataPipeline(BaseEstimator, TransformerMixin):
    """Load and process the KI meta information"""

    def __init__(self):
        self.pipeline = Pipeline(
            [
                ("loader", LoadExcel()),
                ("replace_na", ReplaceValues("na", np.NaN)),
                ("lower_case", LowerCase()),
                ("remove_idx", RemoveColumns(removed_columns="Unnamed: 0")),
                ("remove_profile", RemoveColumns(removed_columns="profile")),
                ("rename_name_w_code", RenameColumn(["name"], ["code"])),
                ("rename_sector", RenameColumn(["Sector"], ["ki_specialization"])),
                (
                    "update_NAN_pilot_AFG",
                    UpdateValue("pilot", "nationalite", "afg", "afghan"),
                ),
                (
                    "update_nigerian_nigerien",
                    UpdateValue("nationalite", "nationalite", "nigerian", "nigerien"),
                ),
                (
                    "update_antre_other",
                    UpdateValue("nationalite", "nationalite", "antre", "other"),
                ),
            ]
        )

    # pylint: disable=invalid-name, unused-argument
    def fit(self, path, y=None):
        """fit the pipeline"""
        self.pipeline.fit(path)
        return self

    def transform(self, path):
        """transform the pipeline"""
        return self.pipeline.transform(path)


class KIScoringDataPipeline(BaseEstimator, TransformerMixin):
    """Load and process the KI scoring data"""

    def __init__(self):

        self.pipeline = Pipeline(
            [
                ("loader", LoadExcel()),
                ("lower_case", LowerCase()),
                ("remove_idx", RemoveColumns(removed_columns="Unnamed: 0")),
                (
                    "rename_population",
                    RenameColumn(["population"], ["question_population"]),
                ),
                ("rename_sector", RenameColumn(["sector"], ["question_sector"])),
                ("remove_duplicates", RemoveDuplicateQuestions()),
            ]
        )

    # pylint: disable=invalid-name, unused-argument
    def fit(self, path, y=None):
        """fit the pipeline"""
        self.pipeline.fit(path)
        return self

    def transform(self, path):
        """transform the pipeline"""
        return self.pipeline.transform(path)


class QuestionsDataPipeline(BaseEstimator, TransformerMixin):
    """Load and process the questions meta information"""

    def __init__(self):

        self.pipeline = Pipeline(
            [
                ("loader", LoadExcel()),
                ("remove_idx", RemoveColumns(removed_columns="Unnamed: 0")),
                ("lower_case", LowerCase()),
                ("rename_variable", RenameColumn(["variable"], ["question"])),
                (
                    "remove_duplicate_columns",
                    RemoveColumns(
                        removed_columns=[
                            "sector",
                            "Statut",
                            "pilot",
                        ]
                    ),
                ),
                ("remove_duplicate_rows", DropDuplicateRows(subset=["question"])),
            ]
        )

    # pylint: disable=invalid-name, unused-argument
    def fit(self, path, y=None):
        """fit the pipeline"""
        self.pipeline.fit(path)
        return self

    def transform(self, path):
        """transform the pipeline"""
        return self.pipeline.transform(path)
