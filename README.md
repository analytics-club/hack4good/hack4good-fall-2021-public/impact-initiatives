# Impact Initiatives Repository: Improving respondent selection for humanitarian research in difficult contexts

IMPACT Initiatives is gathering data about the situation in urban areas that are dealing with refugee crises. Specifically, IMPACT collected data in Jordan, Niger, Afghanistan and Uganda. To improve efficiency, they developed and tested ways of collecting information about refugees and host communities through Key Informants (KI). KI are certain individuals whose position gives them more extensive knowledge about other people or events, when compared to the average person. The purpose of this project is to dig deeper into the reliability of such KI. To this end, IMPACT has collected data comparing information collected directly from households (which for this project will be assumed to be absolutely true) with information given by KI. In detail, data was given about attributes of each KI, a network of KI (describing relationships and communication among one another), questions asked to KI and households, and accuracy of the KI answers to the questions. The goal is to find common characteristics and network positions of KI, as well as certain types of questions that correlate with high accuracy of KI information when compared to household information.

## Installation

1. Install [poetry](https://python-poetry.org) and [Python 3.9](https://www.python.org/downloads/release/python-399/) on your operating system.
2. To install the necessary dependencies run in your terminal/powershell:

```
poetry install
```

## Run the main data pipeline

To create all of the intermediate tables and final data matrices run in your terminal/powershell:

```
poetry run python src/impact/data.py
```

## Modeling

All results and plots of the modeling stage and different approaches are found in the folder `notebooks`.
